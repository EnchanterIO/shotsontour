angular.module('shots', ['ionic', 'ui.bootstrap', 'ngCordova', 'ngStorage', 'shots.controllers', 'shots.services', 'shots.directives'])

.run(function($ionicPlatform, SqlService) {
  SqlService.init();

  $ionicPlatform.ready(function() {

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }

  });
})
.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom');

  $stateProvider.state('shots', {
    abstract: true,
    templateUrl: 'templates/abstract.html'
  })

  .state('shots.game', {
    cache: false,
    url: '/game',
    views: {
      'shots-game': {
        templateUrl: 'templates/game.html',
        controller: 'GameCtrl'
      }
    }
  })

  .state('shots.ranking', {
    url: '/ranking',
    cache: false,
    views: {
      'shots-ranking': {
        templateUrl: 'templates/ranking.html',
        controller: 'RankingCtrl',
        resolve: {
            resultList: function ($q, SqlService) {
                var defered = $q.defer();

                SqlService.getList(SqlService.db).then(function(result) {
                    defered.resolve(result);
                });

                return defered.promise;
            }
        }
      }
    }
  })

  .state('register', {
      url: '/register',
      templateUrl: 'templates/register.html',
      controller: 'RegisterCtrl'
  });

  $urlRouterProvider.otherwise('/register');

});
