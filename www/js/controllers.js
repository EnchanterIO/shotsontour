angular.module('shots.controllers', [])

.controller('GameCtrl', function($scope, $timeout, UserService, SqlService, RankingList) {
    $scope.game = {
        saved: false,
        players: []
    };
    $scope.search = {
        selected: null
    };

    $scope.saveRound = function() {
        if ($scope.game.players.length === 1) {
            return;
        }

        for (var i=0; i<$scope.game.players.length; i++) {
            SqlService.insertShot($scope.game.players[i], 1);
        }

        //SqlService.getList(SqlService.db).then(function(result){
        //    RankingList.setData(result);
        //});

        $scope.displaySuccessMessage();
        $scope.resetGame();
    };

    $scope.addUserToGame = function(player) {
        $scope.search.selected = null;

        if ($scope.game.players.indexOf(player) === -1) {
            $scope.game.players.push(player);
        }
    };

    $scope.unseatPlayer = function(index) {
        $scope.game.players.splice(index, 1);
    };

    $scope.resetGame = function() {
        $scope.game.players = [];

        // Hide success message from view after 2s
        $timeout(function() {
            $scope.game.saved = false;
        }, 2000);

        $scope.addUserToGame(UserService.getActiveUser());
    };

    $scope.displaySuccessMessage = function() {
        $scope.game.saved = true;
    };

    $scope.usersList = UserService.findAll();
    $scope.resetGame();
})

.controller('RankingCtrl', function($scope, $state, RankingList, SqlService, UserService, resultList) {

    $scope.resultList = resultList;

    //RankingList.setScope($scope);
    //SqlService.getList(SqlService.db).then(function(result){
    //    RankingList.setData(result);
    //});

    $scope.syncRanking = function() {
        SqlService.sync(function() {
            SqlService.update().then(function(result) {
                $scope.resultList = result;
            });
        });
    };

    $scope.reset = function() {
        if (confirm('Are u sober? U will reset your local DB')) {
            SqlService.truncateLocal($scope.syncRanking);
            UserService.logout();
            $state.go('register');
        }
    };
})

.controller('RegisterCtrl', function($scope, $state, $localStorage, SqlService, UserService) {
    $scope.error = "";
    $scope.redirect = function() {
        $state.go('shots.game');
    };
    $scope.$storage = $localStorage;
    if (undefined !== $scope.$storage.activeUser && true === $scope.$storage.activeUser.isLoggedIn) {
        $scope.redirect();
    }

    $scope.userList = UserService.findAll();
    $scope.signIn = function($item) {
        if (-1 != UserService.findAll().indexOf($item)) {
            UserService.setActiveUser($item);
            SqlService.syncLocal();
            $scope.redirect();
        } else {
            $scope.error = "If your name is not in the list please try with the email where you got tot notifications."
        }
    };


});
