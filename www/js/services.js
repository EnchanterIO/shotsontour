angular.module('shots.services', [])

.factory('RankingList', function() {
      return {
        //setScope: function($scope){
        //  this.scope = $scope;
        //},
        //setData: function(data){
        //  this.scope.resultList = data;
        //}
      };
})

.factory('UserService', function($localStorage) {
  var users = usernames;
  return {
    findAll: function() {
      return users;
    },
      setActiveUser: function(username) {
          $localStorage.activeUser = {
              username: username,
              isLoggedIn: true
          };
      },

      logout: function()
      {
          $localStorage.activeUser.isLoggedIn = false;
      },

      getActiveUser: function() {
          if(undefined !== $localStorage.activeUser)
          {
              return $localStorage.activeUser.username;
          }
          return undefined;
      }
  };
})

.factory('DropBoxService', function() {
    var key = 'hgryt8p3fulnn0d';
    var token = 'hxjm4FaxTYEAAAAAAAAFnL3cAONXj8Jt_lo39vtB6mXotCKocF_GbafNBjArXHzM';
    var secret = 'c5g3tyvh8ejxwhr';

    var client = new Dropbox.Client({
      'key': key,
      'secret': secret,
      'token': token
    });

    var showError = function(error) {
      switch (error.status) {
        case Dropbox.ApiError.INVALID_TOKEN:
          console.log('Dropbox.ApiError.INVALID_TOKEN');
          break;

        case Dropbox.ApiError.NOT_FOUND:
          console.log('Dropbox.ApiError.NOT_FOUND');
          break;

        case Dropbox.ApiError.OVER_QUOTA:
          console.log('Dropbox.ApiError.OVER_QUOTA');
          break;

        case Dropbox.ApiError.RATE_LIMITED:
          console.log('Dropbox.ApiError.RATE_LIMITED');
          break;

        case Dropbox.ApiError.NETWORK_ERROR:
            alert('Please, connect to Internet.');
          console.log('Dropbox.ApiError.NETWORK_ERROR');
          break;

        case Dropbox.ApiError.INVALID_PARAM:
        case Dropbox.ApiError.OAUTH_ERROR:
        case Dropbox.ApiError.INVALID_METHOD:
          console.log('Dropbox.ApiError.INVALID_METHOD');
        default:
          console.log('Dropbox.ApiError NO IDEA');
      }
    };

    var  getContent = function() {
      client.authenticate(function () {

      });
    };

    return {
      client: client,

      read: function(callback) {
        client.authenticate(function () {
          client.stat('/', {readDir: true}, function (error, list) {
            var files = list._json.contents;
            for (var i=0; i<files.length; i++){
              client.readFile(files[i].path, {}, function (error, fileContent) {
                if (error) {
                  showError(error);
                } else {
                  callback(JSON.parse(fileContent));
                }
              });
            }
          });
        });
      },


      save: function(phoneid, data, callback) {
        client.authenticate(function () {
          client.writeFile(phoneid+'.json', JSON.stringify(data), function (error) {
            if (error) {
              showError(error);
            } else {
              if(typeof callback === 'function')
                callback();
            }
          });
        });
      }
    };
})

.factory('SqlService', function($q, $cordovaSQLite, DropBoxService, UserService) {

  var TableName = 'ShotRecords',
      DbName = 'shots';

  var SqlService = {
      db: null,
      dbLocal: null,

      init: function() {
          if (window.cordova) {
              SqlService.db      = $cordovaSQLite.openDB({name: DbName+".db"});
              SqlService.dbLocal = $cordovaSQLite.openDB({name: DbName+".db_local"});
          }
          else {
              SqlService.db      = window.openDatabase(DbName+"shots.db", "1.0", "My app", -1);
              SqlService.dbLocal = window.openDatabase(DbName+"shots.db_local", "1.0", "My app", -1);
          }

          this.queryParallel("CREATE TABLE IF NOT EXISTS " + TableName + " (phoneid text, username text, amount int)");
      },

      queryParallel: function(query, params) {
          $cordovaSQLite.execute(SqlService.db, query, params).then(function(res) {
              $cordovaSQLite.execute(SqlService.dbLocal, query, params);
              console.log(res);
          }, function (err) {
              console.error(err);
          });
      },

      getList: function(db) {
          var deferred = $q.defer(),
              query = "SELECT username, SUM(amount) as amount FROM " + TableName + " GROUP BY username ORDER BY amount DESC";

          $cordovaSQLite.execute(db, query).then(function(res) {
              var len = res.rows.length;
              var list = [];

              for (var i=0; i<len; i++) {
                  list.push({
                      'username': res.rows.item(i).username,
                      'amount': res.rows.item(i).amount
                  });
              }

              deferred.resolve(list);
          }, function (err) {
              console.error(err);
          });

          return deferred.promise;
      },

      update: function() {
          var defer = $q.defer();

          this.getList(SqlService.db).then(function(result) {
              defer.resolve(result);
          });

          return defer.promise;
      },

      truncateLocal: function(syncCallback) {
          var query = "DELETE FROM " + TableName;
          $cordovaSQLite.execute(SqlService.dbLocal, query).then(function() {
              syncCallback();
          });
      },

      syncLocal: function() {
          var query = "DELETE FROM " + TableName;
          DropBoxService.client.authenticate(function () {
              $cordovaSQLite.execute(SqlService.dbLocal, query).then(function() {
                  DropBoxService.client.readFile(UserService.getActiveUser()+'.json', {}, function (error, fileContent) {
                      if (error) {
                          showError(error);
                      } else {
                          var query = "INSERT INTO " + TableName + " (username, amount) VALUES (?,?)";
                          var response = JSON.parse(fileContent);
                          for(var i=0;i < response.length; i++) {
                              $cordovaSQLite.execute(SqlService.dbLocal, query, [response[i].username, response[i].amount]);
                          }
                      }
                  });
              });
          });
      },

      sync: function(syncCallback) {
          this.getList(SqlService.dbLocal).then(function(result) {
              syncCallback();
              DropBoxService.save(UserService.getActiveUser(), result, function() {
                  var query = "DELETE FROM " + TableName;
                  $cordovaSQLite.execute(SqlService.db, query).then(function() {
                      DropBoxService.read(function(response) {
                          var query = "INSERT INTO " + TableName + " (username, amount) VALUES (?,?)";
                          for(var i=0;i < response.length; i++) {
                              $cordovaSQLite.execute(SqlService.db, query, [response[i].username, response[i].amount]);
                              syncCallback();
                          }
                      });
                  }, function (err) {
                      console.error(err);
                  });
              });
          });
      },

      insertShot: function(username, amount) {
          var query = "INSERT INTO " + TableName + " (phoneid, username, amount) VALUES (?,?,?)";
          this.queryParallel( query, [UserService.getActiveUser(), username, amount]);
          return true;
      }
  };

  return SqlService;
});
